<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!-- controle, iterations, tests, variables -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- traitement XML -->
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<!-- formattage des donnees -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!-- SQL/JDBC -->
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<%@ taglib prefix="ben" uri="/WEB-INF/myTags.tld"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

${personne.prenom} ${personne.nom}
<br/>
<%-- <c:redirect url="index.html"></c:redirect> --%>

<h1><ben:maj  text="${personne.nom}"></ben:maj></h1>

<c:if test="${personne.prenom =='Bob'}">chanteur</c:if>

<ben:liste>
	<ben:listeItem text=""><ben:maj text="lundi"></ben:maj></ben:listeItem>
	<ben:listeItem text="mardi"></ben:listeItem>
	<ben:listeItem text="mercredi"></ben:listeItem>
</ben:liste>


</body>
</html>