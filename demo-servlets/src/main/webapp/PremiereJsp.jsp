<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"
  import="fr.isika.al.entities.*"%>

<%
	Personne p = new Personne();
	p.setNom("Marley");
	p.setPrenom("Bob");
	request.setAttribute("personne", p); 
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1><%=request.getParameter("nom") %></h1>

<h2>${personne.nom}</h2>
<h2><%=((Personne)request.getAttribute("personne")).getPrenom()%></h2>

</body>
</html>