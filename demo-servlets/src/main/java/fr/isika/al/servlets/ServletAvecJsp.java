package fr.isika.al.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.isika.al.entities.Personne;

public class ServletAvecJsp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ServletAvecJsp() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Personne p = (Personne)request.getSession().getAttribute("personne");
		
		if (p == null)
		{
		p =new Personne();
		p.setNom("Dylan");
		p.setPrenom("Bob");
		}
		
		request.setAttribute("personne", p);
		
		this.getServletContext()
				.getRequestDispatcher( "/WEB-INF/jsp/test.jsp" )
				.forward( request, response );
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
