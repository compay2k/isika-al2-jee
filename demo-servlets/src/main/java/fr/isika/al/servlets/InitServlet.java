package fr.isika.al.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.PropertyConfigurator;


public class InitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void init() 
	{
        String cheminWebApp = getServletContext().getRealPath("/");    
        String cheminLogConfig = cheminWebApp + getInitParameter("log4j-fichier-config");
        String cheminLog = cheminWebApp + getInitParameter("log4j-chemin-log");
        File logPathDir = new File( cheminLog );
        System.setProperty( "log.chemin", cheminLog );
        if (cheminLogConfig != null) 
        {
        	PropertyConfigurator.configure(cheminLogConfig);
        }   
    }

}
