package fr.isika.al.servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import fr.isika.al.entities.Personne;

/**
 * Servlet implementation class MyFirstSevlet
 */
public class MyFirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(MyFirstServlet.class);
	ServletConfig config;

	private int taille;

	/**
	 * Default constructor.
	 */
	public MyFirstServlet() {
		System.out.println("Cr�ation de ma servlet");
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.info("OK !!");
		System.out.println("Initialisation de ma servlet");
		this.config = config;
		this.taille = Integer.parseInt(config.getInitParameter("taille"));
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		System.out.println("Destruction de ma servlet");
	}

	/**
	 * @see Servlet#getServletConfig()
	 */
	public ServletConfig getServletConfig() {
		return config;
	}

	/**
	 * @see Servlet#getServletInfo()
	 */
	public String getServletInfo() {
		// TODO Auto-generated method stub

		return null;
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Appel � service de ma servlet");
		super.service(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{

		System.out.println("GET de ma servlet");
	
		response.setContentType("text/html");
		response.getWriter().append("Served at: ").append(request.getContextPath()).append("<br/>taille : " + taille);

		// ServletConfig :
		System.out.println("Parametres config : ");
		for (Enumeration e = config.getInitParameterNames(); e.hasMoreElements();) 
		{
			String param = e.nextElement().toString();
			System.out.println("\t- " + param + " : " + config.getInitParameter(param)); 
 		}
		
		// ServletContext :
		getServletContext().log("plop");
		System.out.println("Attributs context: ");
		for (Enumeration e = getServletContext().getAttributeNames(); e.hasMoreElements();) 
		{
			String param = e.nextElement().toString();
			System.out.println("\t- " + param + " : " + getServletContext().getAttribute(param)); 
 		}
		
		// Cookies :
		Cookie c = new Cookie("couleur", "hotpink");
		c.setMaxAge(3600*24*7);
		response.addCookie(c);
		
		// Session :
		Personne p  = new Personne();
		p.setNom("chauvet");
		p.setPrenom("benoit");
		request.getSession().setAttribute("personne", p); 
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
