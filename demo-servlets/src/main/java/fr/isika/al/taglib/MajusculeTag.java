package fr.isika.al.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class MajusculeTag extends TagSupport{

	private static final long serialVersionUID = 1L;
	
	private String text;
	

	public void setText(String text) {
		this.text = text;
	}


	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			out.print(text.toUpperCase());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}
	
	@Override
	public int doEndTag() throws JspException {
		//return SKIP_PAGE;
		return super.doEndTag();
	}

}
